import {
  Controller,
  Get, Param,
} from '@nestjs/common';
import { createCipheriv, randomBytes } from 'crypto';

@Controller()
export class AppController {

  @Get('/encrypt/:key')
  async encrypt(@Param('key') key) {
    const cipher = createCipheriv('aes-256-ecb', atob(String(process.env.PRIVATE_KEY)), null);
    let encrypted = cipher.update(key, 'utf8', 'base64');
    encrypted += cipher.final('base64');
    return encrypted;
  }
}